﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Homework25
{
    /// <summary>
    /// Interaction logic for EmployeeWindow.xaml
    /// </summary>
    public partial class EmployeeWindow : Window
    {
        public event EventHandler<Employee> AddEmployeeHandler = delegate { };
        public EmployeeWindow()
        {
            InitializeComponent();
        }
        public Employee EmployeeModel { get; set; }
        public void ShowEmployeeModel()
        {
            nameBox.Text = EmployeeModel.Name;
            ageBox.Text = EmployeeModel.Age.ToString();
            salaryBox.Text = EmployeeModel.Salary.ToString();
        }
        private void AddEmployeeButton_Click(object sender, RoutedEventArgs e)
        {
            AddEmployeeHandler.Invoke(sender, new Employee
            {
                Name = nameBox.Text,
                Age = Int32.Parse(ageBox.Text),
                Salary = Int32.Parse(salaryBox.Text)
            });
        }
    }
}
