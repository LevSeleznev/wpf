﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;

namespace Homework25
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ObservableCollection<Employee> employees = new ObservableCollection<Employee>();
        public MainWindow()
        {
            InitializeComponent();
            employees.Add(new Employee() { Name = "Петя", Age = 42, Salary = 25000 });
            employees.Add(new Employee() { Name = "Коля", Age = 39, Salary = 45000 });
            employees.Add(new Employee() { Name = "Иван", Age = 7, Salary = 33000 });
            lvEmployee.ItemsSource = employees;
        }

        private void AddEmployeeButton_Click(object sender, RoutedEventArgs e)
        {
            EmployeeWindow addEmployeeWindow = new EmployeeWindow();
            addEmployeeWindow.Owner = this;
            addEmployeeWindow.AddEmployeeHandler += (s, employee) =>
            {
                employees.Add(employee);
            };
            addEmployeeWindow.EmployeeModel = new Employee();
            addEmployeeWindow.Show();
            addEmployeeWindow.ShowEmployeeModel();
        }

        private void UpdateEmployeeButton_Click(object sender, RoutedEventArgs e)
        {
        }

        private void DeleteEmployeeButton_Click(object sender, RoutedEventArgs e)
        {
        }
    }
}